# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.2/)

## [0.0.0] - YYYY-MM-DD
### This is the first default template for unity packages.