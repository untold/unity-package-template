# About Package Name

Detailed package description

# Using Package Name

The package manual can be found [here] (https://itstheconnection.bit.ai/rdc/WXT4bmXFC0Y2tmPn).

# Technical details
## Requirements

This version of the package is compatible with the following versions of the Unity Editor:

* 2022.1 and later (recommended)

## Package contents

The following table indicates the folder structure of the package:

|Location|Description|
|---|---|
|`<Editor>`|Editor folder.|
|`<Runtime>`|Runtime folder.|

## Document revision history

|Date|Reason|
|---|---|
|Month DD, YYYY|Document created. Matches package version 0.0.0|
|Month DD, YYYY|Document edited. Matches package version 0.0.0|
